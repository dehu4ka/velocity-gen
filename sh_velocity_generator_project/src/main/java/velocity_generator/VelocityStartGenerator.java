// see workaround @ https://youtrack.jetbrains.com/issue/IDEA-291006/Running-the-Java-project-by-using-the-JDK-18-prints-the-garbled-characters-in-console-when-try-printing-the-non-ASCII-characters

package velocity_generator;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

// import org.apache.commons.lang.StringEscapeUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.commons.io.IOUtils;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
//import java.util.Objects;
import org.apache.velocity.runtime.RuntimeConstants;

import org.apache.velocity.tools.generic.DateTool;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class VelocityStartGenerator {
    static String inputTemplate = "Applicant.vm";
    static String outputFile = "output.xml";

    public static void main(String[] args) throws Exception {

        // инициализация
        VelocityEngine velocityEngine = new VelocityEngine();
        // хз как победить, чтоб писал логи.
//        velocityEngine.setProperty("runtime.log", "C:\\temp\\velocity.log");
        // похую, не работает
        velocityEngine.setProperty(RuntimeConstants.RUNTIME_LOG_NAME, "mylog");
        velocityEngine.setProperty(RuntimeConstants.ENCODING_DEFAULT, "UTF-8");
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        velocityEngine.init();
        VelocityContext context = new VelocityContext();

        // JSON из консоли браузера
        InputStream is = Files.newInputStream(Paths.get("context.json"));
        String jsonTxt = IOUtils.toString(is, StandardCharsets.UTF_8);
        // берём ключи scenarioDto -> applicantAnswers
        JSONObject json = new JSONObject(jsonTxt).getJSONObject("scenarioDto").getJSONObject("applicantAnswers");

        // проходимся оп ключам
        for (String keyLvl0 : json.keySet()) {
            System.out.println("===== begin ======");
            Object jsonLvl0 = json.get(keyLvl0);

            if (jsonLvl0 instanceof JSONObject) { // это всегда так
                if ( ((JSONObject)jsonLvl0).has("value") ) { // есть ключ "value"?
//                    System.out.println(jsonLvl0);
                    String nestedStr = ((JSONObject) jsonLvl0).getString("value"); // вложенная в него строка
//                    System.out.println(nestedStr);
                    try { // Если можно из неё создать ещё один JSON - пишем
                        JSONObject nestedJSON = new JSONObject(nestedStr);
//                        System.out.println(nestedJSON);
                        context.put(keyLvl0, nestedJSON);
                        System.out.println("key: "+ keyLvl0 + " value aaa: " + nestedJSON);
                    } catch (JSONException e)  { // нельзя - пишем то что есть.
                        context.put(keyLvl0, ((JSONObject)jsonLvl0).get("value"));
                        //Print key and value
                        System.out.println("key: "+ keyLvl0 + " value bbb: " + jsonLvl0);

                        // Проверяем, если строка содержит [, то это массив
                        if ((((JSONObject) jsonLvl0).toString()).contains("[")) {
                            System.out.println("this is array!");
                            // берём строку массива
                            String nestedStr_arr = ((JSONObject) jsonLvl0).getString("value");
                            // создаём массив
                            JSONArray jArray = new JSONArray(nestedStr_arr);
                            System.out.println(jArray);
                            // и добавляем его в контекст
                            context.put(keyLvl0, jArray);
                        }
                    }
                } else {
                    System.out.println("key: "+ keyLvl0 + " has no value");  // for example key: c1 value: {"visited":true}
                }
            } else {
                System.out.println("NOT JSON instance!!!");  // should never happen
            }
        }

        context.put("dateTool", new DateTool()); // здесь ставить точку останова для дебага если что...

        Writer writer = new FileWriter(new File(outputFile));
//        Velocity.mergeTemplate(inputTemplate, "UTF-8", context, writer);
//        writer.flush();
//        writer.close();
        Template t = Velocity.getTemplate(inputTemplate);

        t.merge(context, writer);
        writer.flush();
        writer.close();

//        String sss = t.getResourceLoader().toString();
//        Object ooob = t.getData();
//        String ssst = t.toString();

        System.out.println("Generated " + outputFile);
    }
}
