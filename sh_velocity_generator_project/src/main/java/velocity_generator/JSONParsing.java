package velocity_generator;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class JSONParsing {
    public static void main(String[] args) throws Exception {
        File f = new File("context.json");
        if (f.exists()) {
            InputStream is = Files.newInputStream(Paths.get("context.json"));
            String jsonTxt = IOUtils.toString(is, StandardCharsets.UTF_8);
            System.out.println(jsonTxt);
            JSONObject json = new JSONObject(jsonTxt);
            String a = json.getString("applicantAnswers");
            System.out.println(a);
        }
    }
}
